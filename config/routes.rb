Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "admin_company#index"
  get "admin_business", to: "admin_business#index"
end
